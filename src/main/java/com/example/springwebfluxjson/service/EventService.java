package com.example.springwebfluxjson.service;

import com.example.springwebfluxjson.model.Event;
import com.example.springwebfluxjson.repositories.EventRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EventService {

    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Flux<Event> getAll() {
        return eventRepository.findAll();
    }

    public Mono<Event> create(Mono<Event> event) {
        return event.flatMap(eventRepository::save);
    }
}
