package com.example.springwebfluxjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebfluxJsonApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebfluxJsonApplication.class, args);
    }

}
