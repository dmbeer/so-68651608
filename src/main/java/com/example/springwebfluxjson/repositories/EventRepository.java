package com.example.springwebfluxjson.repositories;

import com.example.springwebfluxjson.model.Event;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends ReactiveSortingRepository<Event, String> {


}
